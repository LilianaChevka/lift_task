package service;

import model.Lift;
import model.User;

import java.util.ArrayList;
import java.util.List;


public class Main {
    public static void main(String[] args) {

        User user1 = new User("John", 3, 4);
        Lift lift1 = new Lift("#1", 2, 4);
        Lift lift2 = new Lift("#2", 1, 4);
        List<Lift> lifts = new ArrayList<>();
        lifts.add(lift1);
        lifts.add(lift2);
        LiftManager listOfLifts = new LiftManager(lifts);
        listOfLifts.choseTheOptimalLift(user1);



    }
}
