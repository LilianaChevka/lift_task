package service;

import model.Lift;
import model.User;

import java.util.List;

public class LiftManager {
    List<Lift> lifts;

    public LiftManager(List<Lift> lifts) {
        this.lifts = lifts;
    }

    public Lift choseTheOptimalLift( User user) {
        int min = Math.abs(user.getCurrentPosition() - lifts.get(0).getCurrentPosition());
        int index = 0;
        for (int i = 1; i < lifts.size(); i++)
            if (min > Math.abs(user.getCurrentPosition() - lifts.get(i).getCurrentPosition())) {
                min = Math.abs(user.getCurrentPosition() - lifts.get(i).getCurrentPosition());
                index = i;
            }
        return lifts.get(index);
    }
}
