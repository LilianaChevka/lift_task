package model;

public class User {
    private final String name;
    private final int currentPosition;
    private final int toPosition;

    public User(String name, int currentPosition, int toPosition) {
        this.name = name;
        this.currentPosition = currentPosition;
        this.toPosition = toPosition;
    }

    public String getName() {
        return name;
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    public int getToPosition() {
        return toPosition;
    }

    @Override
    public String toString() {
        return "User " + name + " is on the  "
                + currentPosition + " floor "
                + " and he goes to " + toPosition + " floor ";

    }

}
