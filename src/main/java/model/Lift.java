package model;

public class Lift {
    private int currentPosition;
    private String name;
    private int toPosition;


    public Lift(String name, int currentPosition, int toPosition) {
        this.name = name;
        this.currentPosition = currentPosition;
        this.toPosition = toPosition;
    }

    public String getName() {
        return name;
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    public int getToPosition() {
        return toPosition;
    }
    @Override
    public String toString(){
        return "Lift " + getName() + " is the optimal choice";
    }
}
