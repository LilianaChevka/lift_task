import model.Lift;
import model.User;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import service.LiftManager;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.assertEquals;


public class LiftManagerTest {

    private final Lift lift1 = new Lift("#1", 1, 5);
    private final Lift lift2 = new Lift("#2", 2, 10);
    private final Lift lift3 = new Lift("#3", 3, 1);
    private final Lift lift4 = new Lift("#4", 5, 10);
    private final Lift lift5 = new Lift("#5", 8, 1);
    private final Lift lift6 = new Lift("#6", 4, 6);

    private final User user1 = new User("user1", 1, 4);
    private final User user2 = new User("user2", 2, 1);
    private final User user3 = new User("user3", 2, 9);
    private final User user4 = new User("user4", 5, 9);
    private final User user6 = new User("user6", 4, 7);
    private final User user7 = new User("user7", 9, 3);


    private final List<Lift> lifts1 = Arrays.asList(lift1, lift2, lift3);
    private final List<Lift> lifts2 = Arrays.asList(lift4, lift5, lift6);

    private final Lift lift7 = new Lift("#7", 10, 2);
    private final Lift lift8 = new Lift("#8", 3, 7);

    private final User user5 = new User("user5", 7, 2);
    private final List<Lift> lifts3 = Arrays.asList(lift7, lift8);

    @DataProvider(name = "data1")
    public Object[][] getData1() {
        return new Object[][]{
                {lifts1, user1}};
    }

    @DataProvider(name = "data2")
    public Object[][] getData2() {
        return new Object[][]{
                {lifts1, user2}};
    }

    @DataProvider(name = "data3")
    public Object[][] getData3() {
        return new Object[][]{
                {lifts1, user3}};
    }

    @DataProvider(name = "data4")
    public Object[][] getData4() {
        return new Object[][]{
                {lifts2, user4}};
    }

    @DataProvider(name = "data5")
    public Object[][] getData5() {
        return new Object[][]{
                {lifts3, user6}};
    }

    @DataProvider(name = "data6")
    public Object[][] getData6() {
        return new Object[][]{
                {lifts3, user7}};
    }

    @DataProvider(name = "data7")
    public Object[][] getData7() {
        return new Object[][]{
                {lifts3, user5}};
    }

    @Test(dataProvider = "data1")
    public void test1(List<Lift> lifts, User user) {
        LiftManager liftManager = new LiftManager(lifts);
        assertEquals(lift1, liftManager.choseTheOptimalLift(user));
    }

    @Test(dataProvider = "data2")
    public void test2(List<Lift> lifts, User user) {
        LiftManager liftManager = new LiftManager(lifts);
        assertEquals(lift2, liftManager.choseTheOptimalLift(user));
    }

    @Test(dataProvider = "data3")
    public void test3(List<Lift> lifts, User user) {
        LiftManager liftManager = new LiftManager(lifts);
        assertEquals(lift2, liftManager.choseTheOptimalLift(user));
    }

    @Test(dataProvider = "data4")
    public void test4(List<Lift> lifts, User user) {
        LiftManager liftManager = new LiftManager(lifts);
        assertEquals(lift4, liftManager.choseTheOptimalLift(user));
    }

    @Test(dataProvider = "data5")
    public void test5(List<Lift> lifts, User user) {
        LiftManager liftManager = new LiftManager(lifts);
        assertEquals(lift8, liftManager.choseTheOptimalLift(user));
    }

    @Test(dataProvider = "data6")
    public void test6(List<Lift> lifts, User user) {
        LiftManager liftManager = new LiftManager(lifts);
        assertEquals(lift7, liftManager.choseTheOptimalLift(user));
    }

    @Test(dataProvider = "data7")
    public void test7(List<Lift> lifts, User user) {
        LiftManager liftManager = new LiftManager(lifts);
        assertEquals(lift7, liftManager.choseTheOptimalLift(user));
    }
}


